from sqlalchemy import (Boolean, Column, Float, ForeignKey, Integer, Numeric,
                        String)
from sqlalchemy.orm import relationship

from database import Base

COURIER_TABLE = "courier"
ORDER_TABLE = "orders"

class DBCourier(Base):
    __tablename__ = COURIER_TABLE

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    score = Column(Float, default=4.7)
    orders = relationship("DBOrder", back_populates="courier")


class DBOrder(Base):
    __tablename__ = ORDER_TABLE

    id = Column(Integer, primary_key=True)
    courier_id = Column(Integer, ForeignKey(f"{COURIER_TABLE}.id"))
    recipient_score = Column(Integer, default=0)
    sender_score = Column(Integer, default=0)
    time_to_point_A = Column(Integer, nullable=True, default=None)
    time_to_point_B = Column(Integer, nullable=True, default=None)
    accepted_payment_amount = Column(Numeric, nullable=True, default=None)
    is_rejected = Column(Boolean, default=False)

    courier = relationship("DBCourier", back_populates="orders")
