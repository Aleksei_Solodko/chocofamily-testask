import asyncio
import random
from  typing import List
from crud import get_couriers_count
from utils import create_initial_data, get_couriers, make_courier, make_orders, create_order_list, send_in_memory_orders
from models import Courier, InMemoryOrder, InMemoryOrderRequest

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="session")
def event_loop():
    return asyncio.get_event_loop()


@pytest_asyncio.fixture(scope="module")
async def start():
    """
    Create sample data if database is empty. Change best scores for next tests.
    """
    courier_count = await get_couriers_count()
    if courier_count < 40:
        await create_initial_data()
    couriers = await get_couriers()
    for c in couriers:
        if c.score < 4.5:
            break
        await make_orders(c.id, target_score=3, create_orders=50)


@pytest.mark.asyncio
async def test_changing_score(start):
    good_courier = await make_courier("Good Courier")
    await make_orders(good_courier.id, target_score=5, create_orders=10)
    new_courier = await make_courier("New Courier")
    await make_orders(new_courier.id, target_score=4.6, create_orders=10)
    couriers = await get_couriers(5)
    assert couriers[0].id == good_courier.id
    assert couriers[1].id == new_courier.id

    await make_orders(good_courier.id, target_score=4.5)
    await make_orders(new_courier.id, target_score=5)
    couriers = await get_couriers(5)
    assert couriers[0].id == new_courier.id
    assert couriers[1].id == good_courier.id





@pytest.mark.asyncio
async def test_in_memory():
    couriers = [Courier(id=i, name=f"Courier_{i}", score=0) for i in range(41, 61)]
    random.shuffle(couriers)
    order_list: List[InMemoryOrder] = []
    for courier in couriers:
        t_score = 1.2 + random.random() * 3.7
        orders = create_order_list(courier.id, t_score, random.randint(80, 120))
        orders = [InMemoryOrder(courier_name=courier.name, **o.dict()) for o in orders]
        order_list.extend(orders)
    best_courier = Courier(id=542, name=f"The Best Courier", score=0)
    order_list.extend(
        [InMemoryOrder(courier_name=best_courier.name, **o.dict())
         for o in create_order_list(best_courier.id, 5, 150)])
    bad_courier = Courier(id=524, name=f"Bad Courier", score=0)
    order_list.extend(
        [InMemoryOrder(courier_name=bad_courier.name, **o.dict())
         for o in create_order_list(bad_courier.id, 1, 150)])

    req = InMemoryOrderRequest(orders=order_list)
    result = await send_in_memory_orders(req)
    assert result
    assert result[0].id == best_courier.id
    assert result[-1].id == bad_courier.id


