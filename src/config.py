from os import getenv

from dotenv import load_dotenv

load_dotenv()

POSTGRES_ADDRESS = getenv("POSTGRES_ADDRESS", "")
POSTGRES_USER = getenv("POSTGRES_USER", "")
POSTGRES_PASSWORD = getenv("POSTGRES_PASSWORD", "")
POSTGRES_DB = getenv("POSTGRES_DB", "postgres")
db_driver = "postgresql+asyncpg"
DB_CONNECTION_STRING = f"{db_driver}://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_ADDRESS}/{POSTGRES_DB}"
SERVICE_HOST = getenv("SERVICE_HOST", "localhost")
SERVICE_PORT = getenv("SERVICE_PORT", "8000")
SERVICE_ADDR = f"http://{SERVICE_HOST}:{SERVICE_PORT}"

COURIER_PATH = "/courier"
ORDER_PATH = "/order"
