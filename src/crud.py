import sqlalchemy
from sqlalchemy import func
from sqlalchemy.future import select

from database import async_session
from models import CourierIn, Order
from schemas import DBCourier, DBOrder
from utils import calc_courier_score


async def create_courier(courier: CourierIn) -> DBCourier:
    async with async_session() as session:
        async with session.begin():
            db_courier = DBCourier(name=courier.name)
            session.add(db_courier)
            await session.flush()
            await session.refresh(db_courier)
            return db_courier


async def get_couriers_count() -> int:
    async with async_session.begin() as session:
        query = select(func.count(DBCourier.id))
        data = await session.execute(query)
        count = data.scalar()
        return count


async def get_courier(courier_id):
    async with async_session() as session:
        async with session.begin():
            query = select(DBCourier).where(DBCourier.id==courier_id).limit(1)
            data = await session.execute(query)
            result_data = list(data.scalars())
            if result_data:
                return result_data[0]
            else:
                return None


async def get_couriers(offset: int = 0, limit: int = 50):
    async with async_session() as session:
        async with session.begin():
            query = select(DBCourier).order_by(DBCourier.score.desc()).limit(limit).offset(offset)
            data = await session.execute(query)
            result_data = list(data.scalars())
            return result_data


async def get_courier_score(courier_id: int):
    async with async_session.begin() as session:
        try:
            query = select(DBOrder) \
                .where(DBOrder.courier_id == courier_id,
                       DBOrder.is_rejected != True,
                       sqlalchemy.or_(DBOrder.sender_score != 0,
                                      DBOrder.recipient_score != 0)) \
                .order_by(DBOrder.id.desc()).limit(100)
            data = await session.execute(query)
            orm_data = data.scalars()
            score = calc_courier_score(orm_data)
            return score
        except Exception as e:
            print(e)
            return 0


async def update_courier_score(order: DBOrder):
    score = await get_courier_score(order.courier_id)
    if score == 0:
        return
    async with async_session.begin() as session:
        query = select(DBCourier).where(DBCourier.id == order.courier_id)
        data = await session.execute(query)
        courier = list(data.scalars())[0]
        courier.score = score
        session.add(courier)
        await session.flush()


async def create_order(order: Order):
    async with async_session.begin() as session:
        db_order = DBOrder(**order.dict())
        session.add(db_order)
        await session.flush()
        await update_courier_score(db_order)
        return db_order


async def get_orders(offset: int = 0, limit: int = 50):
    async with async_session() as session:
        async with session.begin():
            query = select(DBOrder).order_by(DBOrder.id.desc())\
                .limit(limit).offset(offset)
            data = await session.execute(query)
            result_data = list(data.scalars())
            return result_data
