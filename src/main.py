from typing import List

from fastapi import FastAPI, HTTPException

import crud
from config import COURIER_PATH, ORDER_PATH
from models import Courier, CourierFinal, CourierIn, Order, InMemoryOrderRequest
from utils import make_orders

app = FastAPI()


@app.post(COURIER_PATH, response_model=Courier)
async def add_courier(courier: CourierIn):
    new_courier = await crud.create_courier(courier)
    return new_courier


@app.get(COURIER_PATH + "/{courier_id}", response_model=Courier)
async def get_courier(courier_id: int):
    courier = await crud.get_courier(courier_id)
    if not courier:
        raise HTTPException(status_code=404, detail=f"Courier with id {courier_id} doesn't exist")
    return courier


@app.get(COURIER_PATH, response_model=List[CourierFinal])
async def show_couriers(offset: int = 0, limit: int = 50):
    r = await crud.get_couriers(offset, limit)
    return list(r)


@app.get(ORDER_PATH, response_model=List[Order])
async def get_orders(offset: int = 0, limit: int = 50):
    orders = await get_orders(offset, limit)
    return orders


@app.post(ORDER_PATH, response_model=Order)
async def add_order(order: Order):
    courier = await crud.get_courier(order.courier_id)
    if not courier:
        raise HTTPException(status_code=404, detail=f"Courier with id {order.courier_id} doesn't exist")

    new_order = await crud.create_order(order)
    return new_order


@app.post(ORDER_PATH + "/{courier_id}/set-score")
async def set_score(courier_id: int, score: float = 4.9):
    """
    Test function. Create N orders and change courier score
    :param courier_id: Courier for update
    :param score: New courier score
    :return: Updated courier data
    """
    await make_orders(courier_id, score)
    courier = await crud.get_courier(courier_id)
    return courier


@app.post(ORDER_PATH+"/in_memory", response_model=List[Courier])
async def calc_couriers(orders: InMemoryOrderRequest):
    couriers_score_dict = {}
    courier_list = []
    for order in orders.orders:
        courier_id = order.courier_id
        if courier_id not in couriers_score_dict:
            orders_score = []
            couriers_score_dict[courier_id] = []
            courier_list.append(Courier(id=courier_id, name=order.courier_name, score=0))
        else:
            orders_score = couriers_score_dict[courier_id]
        if order.sender_score != 0:
            orders_score.append(order.sender_score)
        if order.recipient_score != 0:
            orders_score.append(order.recipient_score)

    for courier in courier_list:
        scores = couriers_score_dict[courier.id]
        score_sum = sum(scores)
        score = score_sum / len(scores)
        courier.score = score
    courier_list.sort(key=lambda x: x.score, reverse=True)
    return courier_list




