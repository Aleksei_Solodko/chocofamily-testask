import asyncio
import random
import string
import json
from typing import List

from httpx import AsyncClient

from config import COURIER_PATH, ORDER_PATH, SERVICE_ADDR
from models import CourierFinal, CourierIn, Order, InMemoryOrderRequest, Courier


async def make_courier(name=None) -> CourierFinal:
    if not name:
        name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    courier = CourierIn(name=name)
    async with AsyncClient(base_url=SERVICE_ADDR) as ac:
        resp = await ac.post(COURIER_PATH, data=courier.json())
        j = resp.json()
        courier_full = CourierFinal(**j)
        return courier_full


async def send_order(order: Order)-> Order:
    async with AsyncClient(base_url=SERVICE_ADDR) as ac:
        resp = await ac.post(ORDER_PATH, data=order.json())
        j = resp.json()
        order_resp = Order(**j)
        return order_resp


async def send_orders(orders: List[Order]) -> List[Order]:
    if not orders:
        return []
    tasks = [send_order(o) for o in orders]
    finished = await asyncio.gather(*tasks, return_exceptions=True)
    return finished


def create_order_list(courier_id: int, target_score: float, create_orders: int = 110) -> List[Order]:
    score_sum = 0
    div_by = 0

    def _appr() -> int:
        if score_sum == 0 or div_by == 0:
            return 5
        l_score = score_sum / div_by
        if l_score > target_score:
            val = max(int(target_score - 0.5), 1)
        else:
            val = 5
        return val

    orders = []
    for i in range(create_orders):
        rs = _appr()
        div_by += 1
        score_sum += rs
        cs = _appr()
        div_by += 1
        score_sum += cs
        time_to_a = int(0.5 * random.random() * 60 * 60)
        time_to_b = int(random.random() * 60 * 60)
        amount = random.randint(5, 100) * 1000
        order = Order(recipient_score=rs, sender_score=cs, time_to_point_A=time_to_a, time_to_point_B=time_to_b,
                      accepted_payment_amount=amount, is_rejected=False, courier_id=courier_id)
        orders.append(order)

    return orders


async def make_orders(courier_id: int, target_score: float, create_orders: int = 110):
    orders = []
    results_orders = []
    for i, order in enumerate(create_order_list(courier_id, target_score, create_orders)):
        orders.append(order)
        if i % 10 == 0:
            res = await send_orders(orders)
            results_orders.extend(res)
            orders = []
    res = await send_orders(orders)
    results_orders.extend(res)
    return results_orders


async def create_initial_data():
    target_scores = [i / 100 for i in range(100, 450, 7)]
    target_scores.append(5)
    random.shuffle(target_scores)
    for score in target_scores:
        name = f"{score}_courier"
        print(f'Create orders for {name}')
        courier = await make_courier(name)
        await make_orders(courier.id, score)


async def get_couriers(limit: int=50):
    async with AsyncClient(base_url=SERVICE_ADDR) as ac:
        resp = await ac.get(COURIER_PATH, params={"limit": limit})
        json_data = resp.json()
        result: List[CourierFinal] = [CourierFinal(**d) for d in json_data]
        return result


def calc_courier_score(orders: List[Order]):
    final_score = 0
    div_by = 0
    for order in orders:
        if order.sender_score != 0:
            final_score += order.sender_score
            div_by += 1
        if order.recipient_score != 0:
            final_score += order.recipient_score
            div_by += 1
    if final_score != 0 and div_by != 0:
        final_score = final_score / div_by
    return final_score


async def send_in_memory_orders(orders: InMemoryOrderRequest):
    async with AsyncClient(base_url=SERVICE_ADDR) as ac:
        resp = await ac.post(ORDER_PATH+"/in_memory", data=orders.json())
        if resp.status_code != 200:
            return []
        data = resp.json()
        result = [Courier(**d) for d in data]
        return result
