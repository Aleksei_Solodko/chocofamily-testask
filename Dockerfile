FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && pip install pipenv

ADD Pipfile ./
ADD Pipfile.lock ./
ADD *.sh ./
RUN pipenv install --dev --system --deploy --ignore-pipfile


ADD src ./