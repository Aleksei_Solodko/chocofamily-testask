from typing import Optional, List

from pydantic import BaseModel, validator


class CourierBase(BaseModel):
    name: str

    class Config:
        orm_mode = True


class CourierIn(CourierBase):
    pass


class Courier(CourierBase):
    id: int
    score: float


class CourierFinal(Courier):
    @validator("score")
    def round_check(cls, v):
        return round(v, 1)


class BaseOrder(BaseModel):
    recipient_score: Optional[int]
    sender_score: Optional[int]
    time_to_point_A: int
    time_to_point_B: int
    accepted_payment_amount: int
    is_rejected: bool
    courier_id: int

    class Config:
        orm_mode = True


class InMemoryOrder(BaseOrder):
    courier_name: str


class InMemoryOrderRequest(BaseModel):
    orders: List[InMemoryOrder]


class Order(BaseOrder):
    id: Optional[int]


