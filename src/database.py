import databases
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from config import DB_CONNECTION_STRING

database = databases.Database(DB_CONNECTION_STRING)


engine = create_async_engine(DB_CONNECTION_STRING, future=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()
